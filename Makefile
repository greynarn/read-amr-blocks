DIY_PATH=${HOME}/Research/DIY/diy-mrzv
INCLUDES=-I${DIY_PATH}/include -Iinclude
LIBRARIES=-lmpi
CXXFLAGS=-O3

all: read-blocks fill-simple-block read-npy

read-blocks: read-blocks.cpp fab-block.h
	g++ ${CXXFLAGS} read-blocks.cpp -o $@ ${LIBRARIES} ${INCLUDES} -std=c++11

fill-simple-block: fill-simple-block.cpp fab-block.h
	g++ ${CXXFLAGS} fill-simple-block.cpp -o $@ ${LIBRARIES} ${INCLUDES} -std=c++11

read-npy: read-npy.cpp fab-block.h
	g++ ${CXXFLAGS} read-npy.cpp -o $@ ${LIBRARIES} ${INCLUDES} -std=c++11
