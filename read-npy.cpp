#include <diy/master.hpp>
#include <diy/vertices.hpp>
#include <diy/assigner.hpp>
#include <diy/decomposition.hpp>
#include <diy/link.hpp>
#include <diy/fmt/format.h>
#include <diy/io/block.hpp>         // for saving blocks in DIY format
#include <diy/io/numpy.hpp>

#include <opts/opts.h>

#include "fab-block.h"
using Real  = double;
using Block = FabBlock<Real, 3>;

int main(int argc, char** argv)
{
    using opts::Option;
    using opts::PosOption;
    opts::Options ops;

    bool help;
    int  nblocks = 8;

    ops
        >> Option('b',  "blocks",   nblocks,                        "number of blocks to use")
        >> Option('h',  "help",     help,                           "show help message")
    ;

    std::string infn, outfn;
    if ( !ops.parse(argc, argv) || help || !(ops >> PosOption(infn) >> PosOption(outfn)) )
    {
        fmt::print(std::cerr, "Usage: {} IN.npy OUT.amr\n\n{}\n", argv[0], ops);
        return 1;
    }

    diy::mpi::environment   env(argc, argv);
    diy::mpi::communicator  world;

    diy::mpi::io::file      in(world, infn, diy::mpi::io::file::rdonly);
    diy::io::NumPy          reader(in);
    reader.read_header();

    diy::Master             master(world,
                                   1, -1,
                                   Block::create,
                                   Block::destroy,
                                   0,
                                   Block::save,
                                   Block::load);

    using Decomposer = diy::RegularDecomposer<diy::DiscreteBounds>;
    using Point      = diy::Point<int, DIY_MAX_DIM>;
    diy::DiscreteBounds     domain;
    domain.min = {0,0,0};
    domain.max = reader.shape() - Point::one();
    Decomposer::BoolVector  wrap   { true,  true,  true};               // TODO
    Decomposer decomposer(3, domain, nblocks,
                          Decomposer::BoolVector { false, false, false },   // share_face
                          wrap,
                          Decomposer::CoordinateVector { 1, 1, 1 });        // ghosts

    diy::ContiguousAssigner assigner(world.size(), nblocks);
    decomposer.decompose(0, assigner, [&master,&wrap,&reader](int gid,
                                                const Decomposer::Bounds& core,
                                                const Decomposer::Bounds& bounds,
                                                const Decomposer::Bounds& domain,
                                                const Decomposer::Link&   link)
    {
        auto* b = new Block;

        auto my_bounds = core;

        my_bounds.max += Point::one();
        my_bounds.min -= Point::one();


        // we always want ghosts
        auto         shape_4d = my_bounds.max - my_bounds.min + Point::one();
        Block::Shape shape(&shape_4d[0]);       // quick and hacky
        bool         c_order = true;
        b->fab_storage_ = decltype(b->fab_storage_)(shape, c_order);
        b->fab = decltype(b->fab)(b->fab_storage_.data(), shape, c_order);

        auto core_shape_4d = core.max - core.min + Point::one();
        Block::Shape core_shape(&core_shape_4d[0]);

        diy::Grid<double, 3> core_grid(core_shape);

        reader.read(core, core_grid.data());
        diy::for_each(core_shape, [&](const Block::Shape& p) {
                b->fab_storage_(p + Block::Shape::one()) = core_grid(p);
                });

        // copy link
        diy::AMRLink* amr_link = new diy::AMRLink(3, 0, 1, link.core(), my_bounds);
        for (int i = 0; i < link.size(); ++i)
        {
            amr_link->add_neighbor(link.target(i));
            // shrink core from bounds, since it's not stored in the RegularLink explicitly
            auto nbr_core = link.bounds(i);
            auto nbr_bounds = link.bounds(i);
            nbr_bounds.min -= Point::one();
            nbr_bounds.max += Point::one();
            amr_link->add_bounds(0, 1, nbr_core, nbr_bounds);
        }

        // record wrap
        int dir = 0;
        for (int dir_x : { -1, 0, 1 })
        {
            if (!wrap[0] && dir_x) continue;
            if (dir_x < 0 && core.min[0] != domain.min[0]) continue;
            if (dir_x > 0 && core.max[0] != domain.max[0]) continue;

            for (int dir_y : { -1, 0, 1 })
            {
                if (!wrap[1] && dir_y) continue;
                if (dir_y < 0 && core.min[1] != domain.min[1]) continue;
                if (dir_y > 0 && core.max[1] != domain.max[1]) continue;

                for (int dir_z : { -1, 0, 1 })
                {
                    if (dir_x == 0 and dir_y == 0 and dir_z == 0)
                        continue;

                    if (!wrap[2] && dir_z) continue;
                    if (dir_z < 0 && core.min[2] != domain.min[2]) continue;
                    if (dir_z > 0 && core.max[2] != domain.max[2]) continue;

                    amr_link->add_wrap(diy::Direction { dir_x, dir_y, dir_z });
                }
            }
        }

        master.add(gid, b, amr_link);
    });

    diy::MemoryBuffer       header;
    diy::save(header, domain);
    diy::io::write_blocks(outfn, world, master, header);
}
